module.exports= ({ env }) => ({
    email: {
        provider: 'sendgrid',
        providerOptions: {
            apiKey: env('SENDGRID_API_KEY'),
        },
        settings:{
            defaultFrom: "support@estateyards.in",
            defaultReplyTo: "support@estateyards.in"
        },
    },

}); 