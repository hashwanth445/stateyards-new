const { users } = require("../models/User.settings.json");


module.exports = {
    async getUserItems(ctx) {
        return result1 = await strapi.query('user', 'users-permissions').model.findById(ctx.params.id)
            .populate({
                path: 'items',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: {
                    path: 'users_permissions_user',
                    populate: {
                        path: 'items',
                        populate: {
                            path: 'users_permissions_user',
                            populate: { path: 'items' },
                            select: 'username'
                        },
                        select: 'user'
                    },
                    select: 'username',
                },
                select: 'user'
            })


    }
}