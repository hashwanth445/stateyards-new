
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    find: ctx => {
        return strapi.query('shortlist').find(ctx.query, [
            {
                path: 'listing',
                populate: {
                    path: 'amenities',
                },
            },
            
        ]);
    },

};
