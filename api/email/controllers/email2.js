module.exports = {
    create2: async ctx => {
        await strapi.plugins['email'].services.email.send({
            to:"cleaning.painting@estateyards.in",
            from:"support@estateyards.in",
            replyTo:"support@estateyards.in",
            subject:ctx.request.body.subject,
            text:ctx.request.body.text
        });
        ctx.send("Email sent!");
    }
}