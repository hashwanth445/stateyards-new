module.exports = {
    create4: async ctx => {
        await strapi.plugins['email'].services.email.send({
            to:"constructions@estateyards.in",
            from:"support@estateyards.in",
            replyTo:"support@estateyards.in",
            subject:ctx.request.body.subject,
            text:ctx.request.body.text
        });
        ctx.send("Email sent!");
    }
}