'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    create: async ctx => {
            console.log(ctx.request);
            await strapi.plugins['email'].services.email.send({
                to:"interior@estateyards.in",
                from:"support@estateyards.in",
                replyTo:"support@estateyards.in",
                subject:ctx.request.body.subject,
                text:ctx.request.body.text
            });
            ctx.send("Email sent!");
        }
};
